#include <stdio.h>

int input()
{
    float a;
    printf("Enter a number\n");
    scanf("%f",&a);
    return a;
} 
float compute(float a,float b,float c)
{
	float avg;
	avg=(a+b+c)/3;
	return avg;
}
void output(float a,float b,float c,float avg)
{
	printf(" The average of number a=%f,b=%f and c=%f is %f\n",a,b,c,avg);
}

int main()
{
	float a,b,c,avg;
	a=input();
	b=input();
	c=input();
	avg=compute(a,b,c);
	output(a,b,c,avg);
    return 0;
}
