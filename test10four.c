#include<stdio.h>
void input(int *a,int *b)
{
    printf("Enter a number for a and b\n");
    scanf("%d%d",a,b);
}
int oper1(int *a,int *b)
{
    int s;
    s=(*a)+(*b);
    return s;
}
int oper2(int *a,int *b)
{
    int d;
    d=(*a)-(*b);
    return d;
}
int oper3(int *a,int *b)
{
    int m;
    m=(*a)*(*b);
    return m;
}
int oper4(int *a,int *b)
{
    int di;
    di=(*a)/(*b);
    return di;
}
int oper5(int *a,int *b)
{
    int r;
    r=(*a)%(*b);
    return r;
}
void op1(int *a,int *b,int s)
{
    printf("The addition of two number a=%d and b=%d is %d\n",*a,*b,s);
}
void op2(int *a,int *b,int d)
{
    printf("The subtraction of two number a=%d and b=%d is %d\n",*a,*b,d);
}
void op3(int *a,int *b,int m)
{
    printf("The multiplication of two number a=%d and b=%d is %d\n",*a,*b,m);
}
void op4(int *a,int *b,int di)
{
    printf("The quotient of two number a=%d and b=%d is %d\n",*a,*b,di);
}
void op5(int *a,int *b,int r)
{
    printf("The remainder of two number a=%d and b=%d is %d\n",*a,*b,r);
}
    
int main()
{
    int a,b,s,d,m,di,r;
    input(&a,&b);
    s=oper1(&a,&b);
    d=oper2(&a,&b);
    m=oper3(&a,&b);
    di=oper4(&a,&b);
    r=oper5(&a,&b);
    op1(&a,&b,s);
    op2(&a,&b,d);
    op3(&a,&b,m);
    op4(&a,&b,di);
    op5(&a,&b,r);
    return 0;
}
    
    
    