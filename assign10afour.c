#include<stdio.h>
int input()
{
    int n;
    printf("Enter a number for m and n\n");
    scanf("%d",&n);
    return n;
}
void input1(int m,int n,int f[m][n])
{
     int i,j;
    printf("Enter the number of elements for first matrix\n");
    for(i=0;i<m;i++)
    {
        for(j=0;j<n;j++)
        scanf("%d",&f[i][j]);
    }
}
void input2(int m,int n, int s[m][n])
{
    int i,j;
     printf("Enter number of elements for second matrix\n");
    for(i=0;i<m;i++)
    {
        for(j=0;j<n;j++)
        scanf("%d",&s[i][j]);
    }
}
void compute(int m,int n,int f[m][n],int s[m][n],int sum[m][n])
{
    int i,j;
    for(i=0;i<m;i++)
    {
        for(j=0;j<n;j++)
        {
            sum[i][j]= f[i][j] + s[i][j];
        }
    } 
}
void output(int m, int n,int f[m][n],int s[m][n],int sum[m][n])
{
    int i,j;
    printf("The sum of the matrices are : \n");
    for(i=0;i<m;i++)
    {
        for(j=0;j<n;j++)
        {
             printf("%d\t",sum[i][j]);
        }
        printf("\n");
    }
}
int main()
{
    int m,n;
    m=input();
    n=input();
    int f[m][n],s[m][n],sum[m][n];
    input1(m,n,f);
    input2(m,n,s);
    compute(m,n,f,s,sum);
    output(m,n,f,s,sum);
    return 0;
}