#include<stdio.h>
struct Name
{
	char f_n[50];
	char l_n[50];
};
struct student
{
	int r_no;
	struct Name n;
	char dept[3];
	float fees;
	char sec[2];
	int tot_mar;
};
int main()
{
	struct student s[2];
	int i;
	for(i=0;i<2;i++)
	{
		printf("Enter the details of student %d:\n\n",i+1);
		printf("Enter the roll number\n");
		scanf("%d",&s[i].r_no);
		printf("Enter the student first name\n");
		scanf("%s",s[i].n.f_n);
		printf("Enter the student last name\n");
		scanf("%s",s[i].n.l_n);
		printf("Enter the student department\n");
		scanf("%s",s[i].dept);
		printf("Enter the student section\n");
		scanf("%s",s[i].sec);
		printf("Enter the fees\n");
		scanf("%f",&s[i].fees);
		printf("Enter the total marks obtained\n");
		scanf("%d",&s[i].tot_mar);
	}
	   
	printf("\n\n.......Student details......\n\n");
    for(i=0;i<2;i++)
	{
    	printf("Roll number of student : %d\n",s[i].r_no);
    	printf("First name of the student : %s\n",s[i].n.f_n);
    	printf("Last name of the student :%s\n",s[i].n.l_n);
    	printf("Department student studying : %s\n",s[i].dept);
    	printf("Section of the student : %s\n",s[i].sec);
    	printf("Amout of fees paid by student : %f\n",s[i].fees);
    	printf("The total marks obtained by student : %d\n",s[i].tot_mar);
    			
    	if(s[0].tot_mar>s[1].tot_mar)
        {
        	printf("The highest marks is scored by student-1 : %d\n",s[0].tot_mar);
    	}
        else
    	{
        	printf("The highest marks is scored by student-2 :%d\n",s[1].tot_mar);
    	}
    }
	    return 0;
}