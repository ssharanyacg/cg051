#include <stdio.h>

int input()
{
    int a;
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
} 
int compute(int a,int b,int c)
{
    int large;
    large=(a>b)?(a>c?a:c):(b>c?b:c);
    return large;
}
void output(int a,int b,int c,int d)
{
	printf("the largest of three number a=%d , b=%d and c=%d is %d\n",a,b,c,d);
}

int main()
{
	int a,b,c,d;
	a=input();
	b=input();
	c=input();
	d=compute(a,b,c);
	output(a,b,c,d);
    return 0;
}
