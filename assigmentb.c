#include <stdio.h>
int main() 
{
    int n, rev_num = 0, remainder, org_num;
    printf("Enter an integer: ");
    scanf("%d", &n);
    org_num = n;
    while (n != 0) 
    {
        remainder = n % 10;
        rev_num = rev_num * 10 + remainder;
        n /= 10;
    }

    if (org_num == rev_num)
        printf("%d is a palindrome.", org_num);
    else
        printf("%d is not a palindrome.", org_num);

    return 0;
}