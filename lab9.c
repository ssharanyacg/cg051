#include<stdio.h>
void swap(*int a,int *b, int *temp);
int main()
{
	int num1,num2,temp;
	printf("Enter two numbers\n");
	scanf("%d%d",&num1,&num2);
	printf("The numbers before swapping is num1=%d and num2=%d\n",num1,num2);
	swap(&num1,&num2,&temp);
	printf("The numbers after swapping is num1=%d and num2=%d\n",num1,num2);
	return 0;
}
void swap(int *a,int *b,int *temp)
{
	*temp=*a;
	*a=*b;
	*b=*temp;
}
	
