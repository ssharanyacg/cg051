#include<stdio.h>
void input(int *a,int *b)
{
	printf("Enter a number for a and b\n");
	scanf("%d%d",a,b);
}

void swap(int *a,int *b)
{
	int temp;
	temp=*a;
	*a=*b;
	*b=temp;
}
void output(int *a,int *b)
{
	printf("The swapped number are a=%d\n b=%d\n",*a,*b);
}
int main()
{
	int a,b;
	input(&a,&b);
	swap(&a,&b);
	output(&a,&b);
	return 0;
}