#include<stdio.h>
struct doj
{
	int day;
	char month[20];
	int year;
};
struct employee
{
	int id;
	char name[50];
	float salary;
	struct doj DOJ;
};
int main()
{
	struct employee e1,e2;
	printf("Enter the employee1 details :\n");
	printf("Enter the employee name :\n");
	scanf("%s",e1.name);
	printf("Enter the id :\n");
	scanf("%d",&e1.id);
	printf("Enter the employee salary :\n");
	scanf("%f",&e1.salary);
	printf("Enter the employee date of joining :\n");
	scanf("%d%s%d",&e1.DOJ.day,e1.DOJ.month,&e1.DOJ.year);
	
	printf("Enter the employee2 details :\n");
	printf("Enter the employee name :\n");
	scanf("%s",e2.name);
	printf("Enter the id :\n");
	scanf("%d",&e2.id);
	printf("Enter the employee salary :\n");
	scanf("%f",&e2.salary);
	printf("Enter the employee date of joining :\n");
	scanf("%d%s%d",&e2.DOJ.day,e2.DOJ.month,&e2.DOJ.year);
	
	printf("\n\n......Employee1 details.......\n\n");
	printf("Name of employee : %s\n",e1.name);
	printf("ID number of employee : %d\n",e1.id);
	printf("Salary of the employee : %f\n",e1.salary);
	printf("The Date Of Joining of employee : %d/%s/%d\n",e1.DOJ.day,e1.DOJ.month,e1.DOJ.year);
	
	printf("\n\n.....Employee2 details......\n\n");
	printf("Name of employee : %s\n",e2.name);
	printf("ID number of employee : %d\n",e2.id);
	printf("Salary of the employee : %f\n",e2.salary);
	printf("The Date Of Joining of employee : %d/%s/%d\n",e2.DOJ.day,e2.DOJ.month,e2.DOJ.year);
	return 0;
}
	