#include <stdio.h>

float input()
{
    float a;
    printf("Enter a number\n");
    scanf("%f",&a);
    return a;
}
float interest(float p,float r,float t)
{
	float SI;
    SI = (p*r*t)/100;
    return SI;
}
void output(float p,float r,float t,float SI)
{
	printf("The simple interest of p=%f , t=%f and r=%f is %f\n",p,t,r,SI);
}

int main()
{
	float p,r,t,SI;
	p=input();
	r=input();
	t=input();
	SI=interest(p,t,r);
	output(p,t,r,SI);
    return 0;
}