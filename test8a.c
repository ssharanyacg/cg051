#include<stdio.h>
void input()
{
    char str[100];
    printf("Enter the character\n");
    scanf("%s",str);
}
void output(char str[100])
{
    int len,c,i,flag=0;
    while(str[c]!='\0')
    {
        c++;
        len=c;
    }
    for(i=0;i<len;i++)
    {
        if(str[i]!=str[len-i-1])
        {
            flag=1;
            break;
        }
    }
    if(flag==0)
    {
        printf("%s is a palindrome\n",str);
    }
    else
    {
        printf("%s is not a palindrome\n",str);
    }
}
int main()
{
    char str[100];
    input(str);
    output(str);
    return 0;
}
