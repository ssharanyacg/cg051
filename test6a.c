#include <stdio.h>

int input()
{
    float a;
    printf("Enter a number\n");
    scanf("%f",&a);
    return a;
} 
float compute(float n)
{
	float i,sum=0;
	float avg=0.0;
	for(i=1;i<=n;i++)
	{
		sum=sum+i;
	}
	avg=(float)sum/n;
	return avg;
}
void output(float n,float avg)
{
	printf(" The average of number %f=%f\n",n,avg);
}

int main()
{
	float a,avg;
	a=input();
	avg=compute(a);
	output(a,avg);
    return 0;
}