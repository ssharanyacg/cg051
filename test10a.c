#include<stdio.h>
void add(int *a, int *b, int *s);
void sub(int *a, int *b, int *d);
void mul(int *a, int *b, int *p);
void div(int *a, int *b, int *di);
void rem(int *a, int *b, int *r);
int main()
{
	int num1, num2,s,d,p,di,r;
	printf("Enter a number for two number\n");
	scanf("%d%d",&num1,&num2);
	add(&num1,&num2,&s);
	printf("The sum of two number %d and %d is %d\n",num1,num2,s);
	sub(&num1,&num2,&d);
	printf("The difference of two number %d and %d is %d\n",num1,num2,d);
	mul(&num1,&num2,&p);
	printf("The product of two number %d and %d is %d\n",num1,num2,p);
	div(&num1,&num2,&di);
	printf("The quotient of two number %d and %d is %d\n",num1,num2,di);
	rem(&num1,&num2,&r);
	printf("The remainder of two number %d and %d is %d\n",num1,num2,r);
	return 0;
}
void add(int *a, int *b, int *s)
{
	*s=(*a)+(*b);
}
void sub(int *a, int *b, int *d)
{
	*d=(*a)-(*b);
}
void mul(int *a, int *b, int *p)
{
	*p=(*a)*(*b);
}
void div(int *a, int *b, int *di)
{
	*di=(*a)/(*b);
}
void rem(int *a, int *b, int *r)
{
	*r=(*a)%(*b);
}